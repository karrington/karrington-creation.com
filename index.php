<?php
$lang = $fr_class = $en_class = '';

/* Récupération de la langue dans la chaîne get */
$lang = (isset($_GET['lang']) && file_exists('lang/'.$_GET['lang'].'.json')) ? $_GET['lang'] : 'fr';
/* Définition de la class pour les liens de langue */
if ($lang == 'fr')
    $fr_class = ' class="active"';
else
    $en_class = ' class="active"';
/* Récupération du contenu du fichier .json */
$contenu_fichier_json = file_get_contents('lang/'.$lang.'.json');
/* Les données sont récupérées sous forme de tableau (true) */
$tr = json_decode($contenu_fichier_json, true);
?>

<!DOCTYPE html>
<html  lang="<?php echo $lang ?>">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $tr['head_title'] ?></title>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/a408ea33bd.js" crossorigin="anonymous"></script>
    <meta name="description" content="<?php echo $tr['head_description'] ?>">
    <meta name="author" content="LayoutIt!">

    <link href="css/style.css" rel="stylesheet">
    <style>
        .navbar {
            background: linear-gradient(40deg, rgba(0, 51, 199, .3), rgba(209, 149, 249, .3));
        }
        
        .navbar:not(.top-nav-collapse) {
            background: transparent;
        }
        
        .navbar .navbar-brand img {
            height: 40px;
            margin: 10px;
        }
        
        .hm-gradient {
            background: linear-gradient(40deg, rgba(0, 51, 199, .3), rgba(209, 149, 249, .3));
        }
        
        .heading {
            margin: 0 6rem;
            font-size: 3.8rem;
            font-weight: 700;
            color: #5d4267;
        }
        
        .subheading {
            margin: 2.5rem 6rem;
            color: #bcb2c0;
        }
        
        .btn.btn-margin {
            margin-left: 6rem;
            margin-top: 3rem;
        }
        
        .btn.btn-lily {
            background: linear-gradient(40deg, rgba(0, 51, 199, .7), rgba(209, 149, 249, .7));
            color: #fff;
        }
        
        .title {
            margin-top: 6rem;
            margin-bottom: 2rem;
            color: #5d4267;
        }
        
        .subtitle {
            color: #bcb2c0;
            margin-left: 20%;
            margin-right: 20%;
            margin-bottom: 6rem;
        }
    </style>


</head>

<body class="bg-white">

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark pink scrolling-navbar">
        <a class="navbar-brand" href="#"><strong>Navbar</strong></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Pricing</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Opinions</a>
                </li>
            </ul>
            <ul class="navbar-nav nav-flex-icons">
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-twitter"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fab fa-instagram"></i></a>
                </li>
            </ul>
        </div>
    </nav>


    <div class="row">
        <header>


            <!-- Intro -->
            <section class="view">

                <div class="row">

                    <div class="col-md-6">

                        <div class="d-flex flex-column justify-content-center align-items-center h-100">
                            <h1 class="heading"><?php echo $tr['site_h1'] ?></h1>
                            <h4 class="subheading font-weight-bold">Worldx's most popular framework for building responsive, mobile-first websites and apps</h4>
                            <div class="mr-auto">
                                <button type="button" class="btn btn-lily btn-margin btn-rounded">Get started <i class="fas fa-caret-right ml-3"></i></button>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">

                        <div class="view">
                            <img src="https://images.pexels.com/photos/325045/pexels-photo-325045.jpeg" class="img-fluid" alt="smaple image">
                            <div class="mask flex-center hm-gradient">
                            </div>
                        </div>

                    </div>

                </div>

            </section>
            <!-- Intro -->

        </header>
        <!-- Main navigation -->
    </div>



    <div class="container my-5 z-depth-1">


        <!--Section: Content-->
        <section class="dark-grey-text">

            <div class="row pr-lg-5">
                <div class="col-md-7 mb-4">

                    <div class="view">
                        <img src="https://mdbootstrap.com/img/illustrations/graphics(4).png" class="img-fluid" alt="smaple image">
                    </div>

                </div>
                <div class="col-md-5 d-flex align-items-center">
                    <div>

                        <h3 class="font-weight-bold mb-4">Material Design Blocks</h3>

                        <p>Lorem ipsum dolor sit amet consectetur adip elit. Maiores deleniti explicabo voluptatem quisquam nulla asperiores aspernatur aperiam voluptate et consectetur minima delectus, fugiat eum soluta blanditiis adipisci, velit dolore
                            magnam.
                        </p>

                        <button type="button" class="btn btn-orange btn-rounded mx-0">Download</button>

                    </div>
                </div>
            </div>

        </section>
        <!--Section: Content-->


    </div>
    <div class="container my-5 py-5 z-depth-1">


        <!--Section: Content-->
        <section class="text-center px-md-5 mx-md-5 dark-grey-text">

            <!--Grid row-->
            <div class="row d-flex justify-content-center">

                <!--Grid column-->
                <div class="col-lg-7">

                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores deleniti explicabo voluptatem quisquam nulla asperiores aspernatur aperiam voluptate et consectetur minima delectus, fugiat eum soluta blanditiis adipisci, velit dolore
                        magnam.
                    </p>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->
        </section>
        <!--Section: Content-->


    </div>


    <div class="container my-5 p-5 z-depth-1 unique-color-dark">


        <!--Section: Content-->
        <section class="text-center white-text">

            <!-- Section heading -->
            <h2 class="font-weight-bold mb-4 pb-2 text-uppercase">Features</h2>
            <!-- Section description -->
            <p class="lead mx-auto mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-4 mb-4">

                    <i class="fas fa-brain fa-3x purple-text"></i>
                    <h5 class="font-weight-bold my-4 text-uppercase">Creativity</h5>
                    <p class="mb-md-0 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam minima assumenda deleniti hic.
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 mb-4">

                    <i class="fab fa-sass fa-3x purple-text"></i>
                    <h5 class="font-weight-bold my-4 text-uppercase">Coding</h5>
                    <p class="mb-md-0 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam minima assumenda deleniti hic.
                    </p>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 mb-4">

                    <i class="fas fa-users fa-3x purple-text"></i>
                    <h5 class="font-weight-bold my-4 text-uppercase">Professionalism</h5>
                    <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit maiores aperiam minima assumenda deleniti hic.
                    </p>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </section>
        <!--Section: Content-->


    </div>
    <div class="container my-5">

        <!-- Section -->
        <section>

            <h6 class="font-weight-bold text-center grey-text text-uppercase small mb-4">Services</h6>
            <h3 class="font-weight-bold text-center dark-grey-text pb-2">Our Services</h3>
            <hr class="w-header my-4">
            <p class="lead text-center text-muted pt-2 mb-5">Join thousands of satisfied customers using our template globally.
            </p>

            <div class="row">

                <div class="col-md-6 col-xl-3 mb-4">
                    <div class="card text-center bg-success text-white">
                        <div class="card-body">
                            <p class="mt-4 pt-2"><i class="far fa-object-ungroup fa-4x"></i></p>
                            <h5 class="font-weight-normal my-4 py-2"><a class="text-white" href="#">Web Design</a></h5>
                            <p class="mb-4">He polite be object change. Consider no overcame yourself sociable children.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-3 mb-4">
                    <div class="card text-center">
                        <div class="card-body">
                            <p class="mt-4 pt-2"><i class="fas fa-mobile-alt fa-4x grey-text"></i></p>
                            <h5 class="font-weight-normal my-4 py-2"><a class="dark-grey-text" href="#">Mobile App</a></h5>
                            <p class="text-muted mb-4">He polite be object change. Consider no overcame yourself sociable children.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-3 mb-4">
                    <div class="card text-center deep-purple lighten-1 text-white">
                        <div class="card-body">
                            <p class="mt-4 pt-2"><i class="fas fa-chart-line fa-4x"></i></p>
                            <h5 class="font-weight-normal my-4 py-2"><a class="text-white" href="#">Digital Marketing</a></h5>
                            <p class="mb-4">He polite be object change. Consider no overcame yourself sociable children.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-xl-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <p class="mt-4 pt-2"><i class="fas fa-bullhorn fa-4x grey-text"></i></p>
                            <h5 class="font-weight-normal my-4 py-2"><a class="dark-grey-text" href="#">Branding Package</a></h5>
                            <p class="text-muted mb-4">He polite be object change. Consider no overcame yourself sociable children.</p>
                        </div>
                    </div>
                </div>

            </div>

        </section>
        <!-- Section -->

    </div>




    <div class="row">
        <div class="container my-5">

            <!-- Section -->
            <section>

                <style>
                    .gallery {
                        -webkit-column-count: 3;
                        -moz-column-count: 3;
                        column-count: 3;
                        -webkit-column-width: 33%;
                        -moz-column-width: 33%;
                        column-width: 33%;
                    }
                    
                    .gallery .pics {
                        -webkit-transition: all 350ms ease;
                        transition: all 350ms ease;
                    }
                    
                    .gallery .animation {
                        -webkit-transform: scale(1);
                        -ms-transform: scale(1);
                        transform: scale(1);
                    }
                    
                    @media (max-width: 450px) {
                        .gallery {
                            -webkit-column-count: 1;
                            -moz-column-count: 1;
                            column-count: 1;
                            -webkit-column-width: 100%;
                            -moz-column-width: 100%;
                            column-width: 100%;
                        }
                    }
                    
                    @media (max-width: 400px) {
                        .btn.filter {
                            padding-left: 1.1rem;
                            padding-right: 1.1rem;
                        }
                    }
                    
                    button.close {
                        position: absolute;
                        right: 0;
                        z-index: 2;
                        padding-right: 1rem;
                        padding-top: .6rem;
                    }
                </style>

                <!-- Modal -->
                <div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body p-0">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>

                                <!-- Grid row -->
                                <div class="row">

                                    <!-- Grid column -->
                                    <div class="col-md-6 py-5 pl-5">

                                        <h5 class="font-weight-normal mb-3">Paper cup mockup</h5>

                                        <p class="text-muted">Key letters, explain which writing we he carpeting or fame, the itch expand medical amped through constructing time. And scarfs, gain, get showed accounts decades.</p>

                                        <ul class="list-unstyled font-small mt-5">
                                            <li>
                                                <p class="text-uppercase mb-2"><strong>Client</strong></p>
                                                <p class="text-muted mb-4">Envato Inc.</p>
                                            </li>

                                            <li>
                                                <p class="text-uppercase mb-2"><strong>Date</strong></p>
                                                <p class="text-muted mb-4">June 27, 2019</p>
                                            </li>

                                            <li>
                                                <p class="text-uppercase mb-2"><strong>Skills</strong></p>
                                                <p class="text-muted mb-4">Design, HTML, CSS, Javascript</p>
                                            </li>

                                            <li>
                                                <p class="text-uppercase mb-2"><strong>Address</strong></p>
                                                <a href="https://mdbootstrap.com/docs/jquery/design-blocks/">MDBootstrap</a>
                                            </li>
                                        </ul>

                                    </div>
                                    <!-- Grid column -->

                                    <!-- Grid column -->
                                    <div class="col-md-6">

                                        <div class="view rounded-right">
                                            <img class="img-fluid" src="https://mdbootstrap.com/img/Photos/Vertical/5.jpg" alt="Sample image">
                                        </div>

                                    </div>
                                    <!-- Grid column -->

                                </div>
                                <!-- Grid row -->

                            </div>
                        </div>
                    </div>
                </div>

                <h6 class="font-weight-bold text-center grey-text text-uppercase small mb-4">portfolio</h6>
                <h3 class="font-weight-bold text-center dark-grey-text pb-2">Product Designs</h3>
                <hr class="w-header my-4">
                <p class="lead text-center text-muted pt-2 mb-5">You can find several product design by our professional team in this section.</p>

                <!-- Grid row -->
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-md-12 dark-grey-text d-flex justify-content-center mb-5">

                        <button type="button" class="btn btn-flat btn-lg m-0 px-3 py-1 filter" data-rel="all">All</button>
                        <p class="font-weight-bold mb-0 px-1 py-1">/</p>
                        <button type="button" class="btn btn-flat btn-lg m-0 px-3 py-1 filter" data-rel="1">Design</button>
                        <p class="font-weight-bold mb-0 px-1 py-1">/</p>
                        <button type="button" class="btn btn-flat btn-lg m-0 px-3 py-1 filter" data-rel="2">Mockup</button>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

                <!-- Grid row -->
                <div class="gallery mb-5" id="gallery">

                    <!-- Grid column -->
                    <div class="mb-3 pics all 2 animation">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Others/images/58.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="mb-3 pics animation all 1">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Vertical/7.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="mb-3 pics animation all 1">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Vertical/4.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="mb-3 pics all 2 animation">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Others/images/63.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="mb-3 pics all 2 animation">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Others/images/64.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="mb-3 pics animation all 1">
                        <a data-toggle="modal" data-target="#basicExampleModal">
                            <img class="img-fluid z-depth-1 rounded" src="https://mdbootstrap.com/img/Photos/Vertical/5.jpg" alt="Card image cap">
                        </a>
                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Grid row -->

            </section>
            <!-- Section -->

        </div>
    </div>


    <div class="container my-5 z-depth-1">


        <!--Section: Content-->
        <section class="dark-grey-text p-5">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-5 mb-4 mb-md-0">

                    <div class="view">
                        <img src="https://mdbootstrap.com/img/illustrations/undraw_Group_chat_unwm.svg" class="img-fluid" alt="smaple image">
                    </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-7 mb-lg-0 mb-4">

                    <!-- Form -->
                    <form class="" action="">

                        <!-- Section heading -->
                        <h3 class="font-weight-bold my-3">Subscribe</h3>

                        <p class="text-muted mb-4 pb-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam vitae, fuga similique quos aperiam tenetur quo ut rerum debitis.</p>

                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Enter your email address" aria-label="Enter your email address" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-md btn-primary rounded-right m-0 px-3 py-2 z-depth-0 waves-effect" type="submit" id="button-addon2">Button</button>
                            </div>
                        </div>
                        <small class="form-text black-text"><strong>* Leave your email addres to be notified first.</strong></small>

                    </form>
                    <!-- Form -->

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </section>
        <!--Section: Content-->


    </div>

    <div class="container my-5 py-5 z-depth-1">


        <!--Section: Content-->
        <section class="px-md-5 mx-md-5 text-center text-lg-left dark-grey-text">

            <!--Grid row-->
            <div class="row">

                <!--Grid column-->
                <div class="col-lg-5 col-md-12 mb-0 mb-md-0">

                    <h3 class="font-weight-bold">Contact Us</h3>

                    <p class="text-muted">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Id quam sapiente molestiae numquam quas, voluptates omnis nulla ea odio quia similique corrupti magnam, doloremque laborum.</p>

                    <p><span class="font-weight-bold mr-2">Email:</span><a href="">contact@mycompany.com</a></p>
                    <p><span class="font-weight-bold mr-2">Phone:</span><a href="">+48 123 456 789</a></p>

                </div>
                <!--Grid column-->

                <!--Grid column-->
                <div class="col-lg-7 col-md-12 mb-4 mb-md-0">

                    <!--Grid row-->
                    <div class="row">

                        <!--Grid column-->
                        <div class="col-md-6">

                            <!-- Material outline input -->
                            <div class="md-form md-outline mb-0">
                                <input type="text" id="form-first-name" class="form-control">
                                <label for="form-first-name">First name</label>
                            </div>

                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-md-6">

                            <!-- Material outline input -->
                            <div class="md-form md-outline mb-0">
                                <input type="text" id="form-last-name" class="form-control">
                                <label for="form-last-name">Last name</label>
                            </div>

                        </div>
                        <!--Grid column-->

                    </div>
                    <!--Grid row-->

                    <!-- Material outline input -->
                    <div class="md-form md-outline mt-3">
                        <input type="email" id="form-email" class="form-control">
                        <label for="form-email">E-mail</label>
                    </div>

                    <!-- Material outline input -->
                    <div class="md-form md-outline">
                        <input type="text" id="form-subject" class="form-control">
                        <label for="form-subject">Subject</label>
                    </div>

                    <!--Material textarea-->
                    <div class="md-form md-outline mb-3">
                        <textarea id="form-message" class="md-textarea form-control" rows="3"></textarea>
                        <label for="form-message">How we can help?</label>
                    </div>

                    <button type="submit" class="btn btn-info btn-sm ml-0">Submit<i class="far fa-paper-plane ml-2"></i></button>

                </div>
                <!--Grid column-->

            </div>
            <!--Grid row-->


        </section>
        <!--Section: Content-->


    </div>

    <div class="container z-depth-1 my-5">

        <section class="text-center py-5">

            <p class="mb-4 pb-2 lead font-weight-bold">Trusted by 1 000 000 + developers &amp; designers</p>

            <!-- Logo carousel -->
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" data-interval="1800">
                <div class="carousel-inner">
                    <!-- First slide -->
                    <div class="carousel-item active">
                        <!--Grid row-->
                        <div class="row">

                            <!--First column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/5.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/First column-->

                            <!--Second column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/7.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Second column-->

                            <!--Third column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/6.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Third column-->

                            <!--Fourth column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/9.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Fourth column-->

                        </div>
                        <!--/Grid row-->
                    </div>
                    <!-- First slide -->

                    <!-- Second slide -->
                    <div class="carousel-item">
                        <!--Grid row-->
                        <div class="row">

                            <!--First column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/11.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/First column-->

                            <!--Second column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/10.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Second column-->

                            <!--Third column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/12.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Third column-->

                            <!--Fourth column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/13.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Fourth column-->

                        </div>
                        <!--/Grid row-->
                    </div>
                    <!-- Second slide -->

                    <!-- Third slide -->
                    <div class="carousel-item">
                        <!--Grid row-->
                        <div class="row">

                            <!--First column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/1a.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/First column-->

                            <!--Second column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/2a.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Second column-->

                            <!--Third column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/3a.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Third column-->

                            <!--Fourth column-->
                            <div class="col-lg-3 col-md-6 d-flex align-items-center justify-content-center">
                                <img src="https://mdbootstrap.com/img/Photos/Template/4a.png" class="img-fluid px-4" alt="Logo">
                            </div>
                            <!--/Fourth column-->

                        </div>
                        <!--/Grid row-->
                    </div>
                    <!-- Third slide -->
                </div>

            </div>
            <!-- Logo carousel -->

        </section>

    </div>
    <!--Footer-->



    <footer class="page-footer text-center font-small info-color-dark">

        <div class="rgba-stylish-strong">

            <!--Call to action-->
            <div class="pt-4">
                <a class="btn btn-outline-white" href="https://mdbootstrap.com/docs/jquery/getting-started/download/" target="_blank" role="button">Download
        MDB
        <i class="fas fa-download ml-2"></i>
      </a>
                <a class="btn btn-outline-white" href="https://mdbootstrap.com/education/bootstrap/" target="_blank" role="button">Start
        free tutorial
        <i class="fas fa-graduation-cap ml-2"></i>
      </a>
            </div>
            <!--/.Call to action-->

            <hr class="my-4">

            <!-- Social icons -->
            <div class="pb-4">
                <a href="https://www.facebook.com/mdbootstrap" target="_blank">
                    <i class="fab fa-facebook-f mr-3"></i>
                </a>

                <a href="https://twitter.com/MDBootstrap" target="_blank">
                    <i class="fab fa-twitter mr-3"></i>
                </a>

                <a href="https://www.youtube.com/watch?v=7MUISDJ5ZZ4" target="_blank">
                    <i class="fab fa-youtube mr-3"></i>
                </a>

                <a href="https://plus.google.com/u/0/b/107863090883699620484" target="_blank">
                    <i class="fab fa-google-plus-g mr-3"></i>
                </a>

                <a href="https://dribbble.com/mdbootstrap" target="_blank">
                    <i class="fab fa-dribbble mr-3"></i>
                </a>

                <a href="https://pinterest.com/mdbootstrap" target="_blank">
                    <i class="fab fa-pinterest mr-3"></i>
                </a>

                <a href="https://github.com/mdbootstrap/bootstrap-material-design" target="_blank">
                    <i class="fab fa-github mr-3"></i>
                </a>

                <a href="http://codepen.io/mdbootstrap/" target="_blank">
                    <i class="fab fa-codepen mr-3"></i>
                </a>
            </div>
            <!-- Social icons -->

            <!--Copyright-->
            <div class="footer-copyright py-3">
                © 2019 Copyright:
                <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> MDBootstrap.com </a>
            </div>
            <!--/.Copyright-->

        </div>

    </footer>
    <!--Footer-->
    <!-- Footer -->
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <script>
        document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')
    </script>
</body>

</html>